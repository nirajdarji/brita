package com.industrial.ro.brita.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.industrial.ro.brita.entity.UserToken;

/**
 * @author vishal.p
 *
 */
public interface UserTokenRepository extends MongoRepository<UserToken, String> {

	UserToken findByUserIdAndDeviceToken(String userId, String deviceToken);

	void deleteByUserIdAndDeviceToken(String userId, String deviceToken);
	
	void deleteByDeviceToken(String deviceToken);

	List<UserToken> findByUserIdIn(List<String> userIds);

}