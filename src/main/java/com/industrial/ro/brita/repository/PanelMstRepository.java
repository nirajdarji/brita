package com.industrial.ro.brita.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.industrial.ro.brita.entity.PanelMst;

public interface PanelMstRepository extends MongoRepository<PanelMst, String> {

	List<PanelMst> findBySid(String sid);

	Optional<PanelMst> findByPidAndIsAllocated(String pid, Boolean isAllocated);

	List<PanelMst> findByIsAllocated(Boolean isAllocated);

}
