package com.industrial.ro.brita.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.industrial.ro.brita.entity.UserInformation;

/**
 * @author vishal.p
 *
 */
public interface UserRepository extends MongoRepository<UserInformation, String> {

	UserInformation findByUserName(String userName);

	List<UserInformation> findByCreator(String creator);

	List<UserInformation> findByUserRole(String name);

	List<UserInformation> findByUserRoleAndCreator(String userRole, String userId);

	UserInformation findByPanelIds(String panelIds);
}
