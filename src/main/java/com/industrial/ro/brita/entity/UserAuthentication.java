package com.industrial.ro.brita.entity;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author vishal.p
 *
 */
public class UserAuthentication extends AbstractAuthenticationToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected Long tokenCreateTime;
	protected String userId;
	protected String role;
	protected String userName;
	protected String deviceToken;

	public UserAuthentication(Collection<? extends GrantedAuthority> authorities, Long tokenCreateTime, String userId,
			String role, String userName, String deviceToken) {
		super(authorities);
		this.tokenCreateTime = tokenCreateTime;
		this.userId = userId;
		this.role = role;
		this.userName = userName;
		this.deviceToken = deviceToken;
		super.setAuthenticated(true);

	}

	public Long getTokenCreateTime() {
		return tokenCreateTime;
	}

	public void setTokenCreateTime(Long tokenCreateTime) {
		this.tokenCreateTime = tokenCreateTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return null;
	}

	@Override
	public String toString() {
		return "UserAuthentication [tokenCreateTime=" + tokenCreateTime + ", userId=" + userId + ", role=" + role
				+ ", userName=" + userName + ", deviceToken=" + deviceToken + "]";
	}

}
