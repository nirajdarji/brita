package com.industrial.ro.brita.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.industrial.ro.brita.common.entity.EntityName;

/**
 * @author vishal.p
 *
 */
@Document(collection = EntityName.USER)
@TypeAlias(EntityName.USER)
@JsonInclude(Include.NON_NULL)
public class LogInResponse {

	@Id
	private String userId;
	private String userName;
	private String userEmail;
	@Field("userRole")
	private String userRole;
	private String jwtToken;
	private List<String> panelIds;

	public LogInResponse() {
	}

	public LogInResponse(String userId, String userName, String userRole, String jwtToken, List<String> panelIds) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userRole = userRole;
		this.jwtToken = jwtToken;
		this.panelIds = panelIds;
	}
	
	public LogInResponse(String userId, String userName, String userRole,  List<String> panelIds) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userRole = userRole;
		this.panelIds = panelIds;
	}

	public LogInResponse(String userId, String userName, String userEmail, String userRole, String jwtToken,
			List<String> panelIds) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userEmail = userEmail;
		this.userRole = userRole;
		this.jwtToken = jwtToken;
		this.panelIds = panelIds;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getJwtToken() {
		return jwtToken;
	}

	public void setJwtToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}

	public List<String> getPanelIds() {
		return panelIds;
	}

	public void setPanelIds(List<String> panelIds) {
		this.panelIds = panelIds;
	}

}
