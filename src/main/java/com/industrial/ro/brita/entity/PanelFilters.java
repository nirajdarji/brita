package com.industrial.ro.brita.entity;

public class PanelFilters {

	private String pid;
	private Boolean isAllocated;

	public PanelFilters() {
		super();
	}

	public PanelFilters(String pid, Boolean isAllocated) {
		super();
		this.pid = pid;
		this.isAllocated = isAllocated;
	}

	public PanelFilters(Boolean isAllocated) {
		super();
		this.isAllocated = isAllocated;
	}

	public PanelFilters(String pid) {
		super();
		this.pid = pid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Boolean getIsAllocated() {
		return isAllocated;
	}

	public void setIsAllocated(Boolean isAllocated) {
		this.isAllocated = isAllocated;
	}

	@Override
	public String toString() {
		return "PanelFilters [" + (pid != null ? "pid=" + pid + ", " : "")
				+ (isAllocated != null ? "isAllocated=" + isAllocated + ", " : "") + "]";
	}

}
