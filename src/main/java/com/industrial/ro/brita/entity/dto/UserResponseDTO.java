package com.industrial.ro.brita.entity.dto;
public class UserResponseDTO {
    String content;

    public UserResponseDTO() {
    }

    public String getContent() {
        return content;
    }

    public UserResponseDTO(String content) {
        this.content = content;
    }
}