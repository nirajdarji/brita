package com.industrial.ro.brita.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.industrial.ro.brita.common.entity.EntityName;

@Document(collection = EntityName.USER_TOKEN)
@TypeAlias(EntityName.USER_TOKEN)
// @CompoundIndexes({ @CompoundIndex(name = "my_index_name", unique = true, def
// = "{'deviceToken' : 1, 'userId' : 1}") })
public class UserToken {

	@Id
	private String userTokenId;
	private String userId;
	private String jwtToken;
	private String deviceToken;
	private Date createdAt;
	private Date expiredAt;
	private String status;

	public UserToken() {
	}

	public UserToken(String userTokenId, String userId, String jwtToken, String deviceToken) {
		super();
		this.userTokenId = userTokenId;
		this.userId = userId;
		this.jwtToken = jwtToken;
		this.deviceToken = deviceToken;
	}

	public UserToken(String userId, String jwtToken, String deviceToken) {
		super();
		this.userId = userId;
		this.jwtToken = jwtToken;
		this.deviceToken = deviceToken;
	}
	
	public UserToken(String userId, String deviceToken) {
		super();
		this.userId = userId;
		this.deviceToken = deviceToken;
	}
	
	

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getJwtToken() {
		return jwtToken;
	}

	public void setJwtToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getExpiredAt() {
		return expiredAt;
	}

	public void setExpiredAt(Date expiredAt) {
		this.expiredAt = expiredAt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserTokenId() {
		return userTokenId;
	}

	public void setUserTokenId(String userTokenId) {
		this.userTokenId = userTokenId;
	}

	@Override
	public String toString() {
		return "UserToken [jwtToken=" + jwtToken + "]";
	}

}
