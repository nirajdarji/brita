package com.industrial.ro.brita.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Document(collection = "panel_mst")
@TypeAlias("panel_mst")
@JsonInclude(Include.NON_NULL)
public class PanelMst {

	@Id
	private String pid;
	private String pn;
	private String pst;
	private String pop;
	private String ovv;
	private String uvv;
	private String roc;
	private String huc;
	private String ruc;
	private String hoc;
	private String alt;
	private String rwt;
	private String rjt;
	private String pwt;
	private String rtd;
	private String ptd;

	private String lpss;
	private String hpss;
	private String dls;
	private String rwtls;
	private String twtls;
	private String us;
	private String ai1a;
	private String ai2s;
	private String ai3s;
	private String rwps;
	private String hpps;
	private String aos;
	private String poos;
	private String totwup;
	private String torwup;
	private String pwfrul;
	private String irwfrul;
	private String orwfrul;
	private String pwvuk;
	private String irwvuk;
	private String orwvuk;
	private Integer tnocop;
	private Double rwcu;
	private Double hppcu;
	private String svu;
	private String sca;
	private String nsip;

	private String cn;
	private String lop;
	private Boolean raoo;

	private String sid;
	private List<String> eid;
	private Boolean isAllocated;
	private String im;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	public String getPst() {
		return pst;
	}

	public void setPst(String pst) {
		this.pst = pst;
	}

	public String getPop() {
		return pop;
	}

	public void setPop(String pop) {
		this.pop = pop;
	}

	public String getOvv() {
		return ovv;
	}

	public void setOvv(String ovv) {
		this.ovv = ovv;
	}

	public String getUvv() {
		return uvv;
	}

	public void setUvv(String uvv) {
		this.uvv = uvv;
	}

	public String getRoc() {
		return roc;
	}

	public void setRoc(String roc) {
		this.roc = roc;
	}

	public String getHuc() {
		return huc;
	}

	public void setHuc(String huc) {
		this.huc = huc;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getHoc() {
		return hoc;
	}

	public void setHoc(String hoc) {
		this.hoc = hoc;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public String getRwt() {
		return rwt;
	}

	public void setRwt(String rwt) {
		this.rwt = rwt;
	}

	public String getRjt() {
		return rjt;
	}

	public void setRjt(String rjt) {
		this.rjt = rjt;
	}

	public String getPwt() {
		return pwt;
	}

	public void setPwt(String pwt) {
		this.pwt = pwt;
	}

	public String getRtd() {
		return rtd;
	}

	public void setRtd(String rtd) {
		this.rtd = rtd;
	}

	public String getPtd() {
		return ptd;
	}

	public void setPtd(String ptd) {
		this.ptd = ptd;
	}

	public String getLpss() {
		return lpss;
	}

	public void setLpss(String lpss) {
		this.lpss = lpss;
	}

	public String getHpss() {
		return hpss;
	}

	public void setHpss(String hpss) {
		this.hpss = hpss;
	}

	public String getDls() {
		return dls;
	}

	public void setDls(String dls) {
		this.dls = dls;
	}

	public String getRwtls() {
		return rwtls;
	}

	public void setRwtls(String rwtls) {
		this.rwtls = rwtls;
	}

	public String getTwtls() {
		return twtls;
	}

	public void setTwtls(String twtls) {
		this.twtls = twtls;
	}

	public String getUs() {
		return us;
	}

	public void setUs(String us) {
		this.us = us;
	}

	public String getAi1a() {
		return ai1a;
	}

	public void setAi1a(String ai1a) {
		this.ai1a = ai1a;
	}

	public String getAi2s() {
		return ai2s;
	}

	public void setAi2s(String ai2s) {
		this.ai2s = ai2s;
	}

	public String getAi3s() {
		return ai3s;
	}

	public void setAi3s(String ai3s) {
		this.ai3s = ai3s;
	}

	public String getRwps() {
		return rwps;
	}

	public void setRwps(String rwps) {
		this.rwps = rwps;
	}

	public String getHpps() {
		return hpps;
	}

	public void setHpps(String hpps) {
		this.hpps = hpps;
	}

	public String getAos() {
		return aos;
	}

	public void setAos(String aos) {
		this.aos = aos;
	}

	public String getPoos() {
		return poos;
	}

	public void setPoos(String poos) {
		this.poos = poos;
	}

	public String getTotwup() {
		return totwup;
	}

	public void setTotwup(String totwup) {
		this.totwup = totwup;
	}

	public String getTorwup() {
		return torwup;
	}

	public void setTorwup(String torwup) {
		this.torwup = torwup;
	}

	public String getPwfrul() {
		return pwfrul;
	}

	public void setPwfrul(String pwfrul) {
		this.pwfrul = pwfrul;
	}

	public String getIrwfrul() {
		return irwfrul;
	}

	public void setIrwfrul(String irwfrul) {
		this.irwfrul = irwfrul;
	}

	public String getOrwfrul() {
		return orwfrul;
	}

	public void setOrwfrul(String orwfrul) {
		this.orwfrul = orwfrul;
	}

	public String getPwvuk() {
		return pwvuk;
	}

	public void setPwvuk(String pwvuk) {
		this.pwvuk = pwvuk;
	}

	public String getIrwvuk() {
		return irwvuk;
	}

	public void setIrwvuk(String irwvuk) {
		this.irwvuk = irwvuk;
	}

	public String getOrwvuk() {
		return orwvuk;
	}

	public void setOrwvuk(String orwvuk) {
		this.orwvuk = orwvuk;
	}

	public Integer getTnocop() {
		return tnocop;
	}

	public void setTnocop(Integer tnocop) {
		this.tnocop = tnocop;
	}

	public Double getRwcu() {
		return rwcu;
	}

	public void setRwcu(Double rwcu) {
		this.rwcu = rwcu;
	}

	public Double getHppcu() {
		return hppcu;
	}

	public void setHppcu(Double hppcu) {
		this.hppcu = hppcu;
	}

	public String getSvu() {
		return svu;
	}

	public void setSvu(String svu) {
		this.svu = svu;
	}

	public String getSca() {
		return sca;
	}

	public void setSca(String sca) {
		this.sca = sca;
	}

	public String getNsip() {
		return nsip;
	}

	public void setNsip(String nsip) {
		this.nsip = nsip;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getLop() {
		return lop;
	}

	public void setLop(String lop) {
		this.lop = lop;
	}

	public Boolean getRaoo() {
		return raoo;
	}

	public void setRaoo(Boolean raoo) {
		this.raoo = raoo;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public List<String> getEid() {
		return eid;
	}

	public void setEid(List<String> eid) {
		this.eid = eid;
	}

	public Boolean getIsAllocated() {
		return isAllocated;
	}

	public void setIsAllocated(Boolean isAllocated) {
		this.isAllocated = isAllocated;
	}

	public String getIm() {
		return im;
	}

	public void setIm(String im) {
		this.im = im;
	}

}