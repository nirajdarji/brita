package com.industrial.ro.brita.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.industrial.ro.brita.common.entity.EntityName;

/**
 * @author LENOVO-PC
 *
 */
@Document(collection = EntityName.USER)
@TypeAlias(EntityName.USER)
@JsonInclude(Include.NON_NULL)
public class UserInformation {

	@Id
	private String userId;
	// @Indexed(unique=true)
	private String userName;
	// @Indexed(unique=true)
	private String userEmail;
	private String userPassword;
	@Field("userRole")
	private String userRole;
	private String creator;
	private String createdDate;
	private Date modifiedDate;
	private String deviceToken;
	private List<String> panelIds;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public List<String> getPanelIds() {
		return panelIds;
	}

	public void setPanelIds(List<String> panelIds) {
		this.panelIds = panelIds;
	}

}
