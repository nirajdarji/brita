package com.industrial.ro.brita.entity;

public class PanelInfo {

	private PanelMst panel;
	private LogInResponse user;

	public PanelMst getPanel() {
		return panel;
	}

	public void setPanel(PanelMst panel) {
		this.panel = panel;
	}

	public LogInResponse getUser() {
		return user;
	}

	public void setUser(LogInResponse user) {
		this.user = user;
	}

	public PanelInfo(PanelMst panel, LogInResponse user) {
		super();
		this.panel = panel;
		this.user = user;
	}

	public PanelInfo(PanelMst panel) {
		super();
		this.panel = panel;
	}

}
