package com.industrial.ro.brita.entity.dto;

public class PanelDTO {

	private String pn;
	private String pi;

	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	public String getPi() {
		return pi;
	}

	public void setPi(String pi) {
		this.pi = pi;
	}

}
