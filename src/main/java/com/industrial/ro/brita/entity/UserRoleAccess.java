package com.industrial.ro.brita.entity;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.industrial.ro.brita.common.entity.EntityName;

@Document(collection = EntityName.USER_ROLE_ACCESS)
@TypeAlias(EntityName.USER_ROLE_ACCESS)
public class UserRoleAccess {

	private String roleName;
	private boolean addUpdateDeleteRestaurant;
	private boolean addUpdateDeleteDriver;

	public boolean isAddUpdateDeleteRestaurant() {
		return addUpdateDeleteRestaurant;
	}

	public void setAddUpdateDeleteRestaurant(boolean addUpdateDeleteRestaurant) {
		this.addUpdateDeleteRestaurant = addUpdateDeleteRestaurant;
	}

	public boolean isAddUpdateDeleteDriver() {
		return addUpdateDeleteDriver;
	}

	public void setAddUpdateDeleteDriver(boolean addUpdateDeleteDriver) {
		this.addUpdateDeleteDriver = addUpdateDeleteDriver;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public String toString() {
		return "UserRoleAccess [roleName=" + roleName + ", addUpdateDeleteRestaurant=" + addUpdateDeleteRestaurant
				+ ", addUpdateDeleteDriver=" + addUpdateDeleteDriver + "]";
	}
}
