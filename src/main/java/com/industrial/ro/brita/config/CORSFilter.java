package com.industrial.ro.brita.config;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author vishal.p
 *
 */
public class CORSFilter implements Filter {



    @Override
    public void doFilter(ServletRequest request, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest req = (HttpServletRequest) request;
        response.setHeader("Access-Control-Allow-Origin", req.getHeader("origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with,origin, content-type, Accept,Authorization");
		response.setHeader("Access-Control-Expose-Headers", "Authorization, Content-Disposition ");
		if ("OPTIONS".equalsIgnoreCase(req.getMethod())) {
			response.setStatus(HttpServletResponse.SC_OK);
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        // Nothing to init
    }

    @Override
    public void destroy() {
        // Nothing to destroy
    }

}
