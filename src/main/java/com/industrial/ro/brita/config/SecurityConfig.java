package com.industrial.ro.brita.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.session.SessionManagementFilter;

/**
 * @author vishal.p
 *
 */
@SuppressWarnings("deprecation")
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomAuthenticationProvider authProvider;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().addFilterBefore(corsFilter(), SessionManagementFilter.class);
		http.authorizeRequests().antMatchers("/saveorupdateuser", "/devicelogout", "/users", "/userbyid",
				"/saveorupdatepanel", "/panels", "/panelbyid").fullyAuthenticated().and().httpBasic();

	}

	@SuppressWarnings("squid:CallToDeprecatedMethod")
	@Bean
	public static NoOpPasswordEncoder passwordEncoder() {
		return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
	}

	@Bean
	public CORSFilter corsFilter() {
		return new CORSFilter();
	}
}
