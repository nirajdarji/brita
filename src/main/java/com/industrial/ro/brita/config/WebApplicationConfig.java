package com.industrial.ro.brita.config;

import static com.industrial.ro.brita.common.utils.BritaConstants.BACKSLASH_NOT_FOUND;
import static com.industrial.ro.brita.common.utils.BritaConstants.FORWARD_INDEX_HTML;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebApplicationConfig implements WebMvcConfigurer {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController(BACKSLASH_NOT_FOUND).setViewName(FORWARD_INDEX_HTML);
	}

	@Bean
	public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> containerCustomizer() {
		return container -> {
			container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, BACKSLASH_NOT_FOUND));
		};
	}

}