package com.industrial.ro.brita.config;

import static java.util.Objects.isNull;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.industrial.ro.brita.entity.UserInformation;
import com.industrial.ro.brita.repository.UserRepository;

/**
 * @author vishal.p
 *
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	UserRepository userRepository;

	@Override
	public Authentication authenticate(Authentication authentication) {
		String name = authentication.getName();
		String password = authentication.getCredentials().toString();
		if (validateUserNameAndPassword(name, password)) {
			return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
		} else {
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	public boolean validateUserNameAndPassword(String userName, String password) {
		UserInformation userInfo = userRepository.findByUserName(userName);
		if (!isNull(userInfo) && userName.equals(userInfo.getUserName())
				&& password.equals(userInfo.getUserPassword())) {
			return true;
		}
		SecurityContextHolder.getContext().setAuthentication(null);
		return false;
	}

}
