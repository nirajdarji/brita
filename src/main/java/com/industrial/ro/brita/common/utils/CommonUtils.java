package com.industrial.ro.brita.common.utils;

import java.util.Collection;

public final class CommonUtils {

	public CommonUtils() {
	}

	public static boolean isNullEmpty(final Collection<?> c) {
		return (c == null || c.isEmpty());
	}

}
