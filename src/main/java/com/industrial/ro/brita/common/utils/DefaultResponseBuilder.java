package com.industrial.ro.brita.common.utils;

import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;

import static  com.industrial.ro.brita.common.entity.ResponseMessageCodes.CREATED;
import static  com.industrial.ro.brita.common.entity.ResponseMessageCodes.SUCCESS;


//import java.io.BufferedInputStream;
//import java.io.InputStream;
//import java.io.OutputStream;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.industrial.ro.brita.common.entity.DefaultResponse;



/**
 * @author bhargav.m. DefaultResponseBuilder class is utility for generating default response.
 *
 */
public final class DefaultResponseBuilder {

    private static final String ATTACHMENT_FILENAME = "attachment; filename=";

    private DefaultResponseBuilder() {
    }

    public static DefaultResponse success() {
        return new DefaultResponse(SUCCESS.getCode(), HttpStatus.OK);
    }

    public static DefaultResponse created() {
        return new DefaultResponse(CREATED.getCode(), HttpStatus.CREATED);
    }

    public static DefaultResponse badRequest(int code) {
        return new DefaultResponse(code, HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<StreamingResponseBody> createStreamingResponseWithFile(String filename, StreamingResponseBody streamingResponseBody) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + filename);
        headers.add(CONTENT_TYPE, APPLICATION_OCTET_STREAM_VALUE);
        return ResponseEntity.ok().headers(headers).body(streamingResponseBody);
    }

    public static ResponseEntity<StreamingResponseBody> createStreamingResponseWithFileAndResponseCode(String filename,
            StreamingResponseBody streamingResponseBody, Integer statusCode) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + filename);
        headers.add(CONTENT_TYPE, APPLICATION_OCTET_STREAM_VALUE);
        return ResponseEntity.status(statusCode).headers(headers).body(streamingResponseBody);
    }

    public static ResponseEntity<StreamingResponseBody> createStreamingResponseWithFileContentType(String filename, String contentType,
            StreamingResponseBody streamingResponseBody) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + filename);
        headers.add(CONTENT_TYPE, contentType);
        return ResponseEntity.ok().headers(headers).body(streamingResponseBody);
    }

    public static ResponseEntity<StreamingResponseBody> createStreamingResponseWithViewFile(String filename, String contentType,
            StreamingResponseBody streamingResponseBody) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_DISPOSITION, "inline; filename=" + filename);
        headers.add(CONTENT_TYPE, contentType);
        return ResponseEntity.ok().headers(headers).body(streamingResponseBody);
    }

    public static ResponseEntity<StreamingResponseBody> createStreamingResponseWithViewFileAndHeaders(String filename, String contentType,
            StreamingResponseBody streamingResponseBody, Map<String, String> headers) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(CONTENT_DISPOSITION, "inline; filename=" + filename);
        httpHeaders.add(CONTENT_TYPE, contentType);
        headers.forEach(httpHeaders::add);
        return ResponseEntity.ok().headers(httpHeaders).body(streamingResponseBody);
    }

	/*
	 * public static StreamingResponseBody getStreamingResponseBody(InputStream
	 * objectInputStream) { return (OutputStream outputStream) -> { try
	 * (BufferedInputStream inputStream = new
	 * BufferedInputStream(objectInputStream)) { IOUtils.fastCopy(inputStream,
	 * outputStream); } }; }
	 */

}