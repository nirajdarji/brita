package com.industrial.ro.brita.common.exception;

public class CustomException extends Exception {

	private static final long serialVersionUID = 1L;

	private String errorMessage;
	private Integer errorCode;

	public CustomException(ErrorCodes errorCode, String errorMessage) {
		this.errorCode = errorCode.getErrorCode();
		this.errorMessage = errorMessage;
	}

	public CustomException(ErrorCodes errorCode) {
		this.errorCode = errorCode.getErrorCode();
		this.errorMessage = errorCode.getDescription();

	}

	public CustomException(Throwable cause) {
		super(cause);
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public CustomException() {
	}

}
