package com.industrial.ro.brita.common.exception;

public enum ErrorCodes {

	// Response code for success response
	RMS_TRANSACTION_OK(0, "Action completed successfully."),
	RMS_VERSION_UPDATE(100, "New Version available, Please update the version"),

	// Response code for general errors [start Range: 1000]
	RMS_INTERNAL(1001, "Internal error occure. Please contact administrator."),
	RMS_SERVICE_UNAVAILABLE(1002, "Requested service unavailable"),
	RMS_SERVICE_NOT_FOUND(1003, "Requested service not found"), RMS_REQUEST_TIMEOUT(1004, "Request timeout"),
	RMS_REQUEST_NOT_AUTHORIZE(1005, "Requested service not authorize"),
	RMS_ACTION_UNSUPPORTED(1006, "Action or method Unsupported"), RMS_FILE_UPLOAD(1007, "File Upload failed"),
	RMS_FILE_UPLOAD_FAILED(1008, "File Upload Exception"),

	// Response code for database errors [start Range: 2000]
	RMS_DATABASE_GENERAL(2001, "General exception for common Database error"),
	RMS_CONNECTION_DOWN(2002, "Database connection not available at the momnent"),
	RMS_DB_CONSTRAINT_VIOLATION_EXCEPTION(2003, " Database CONSTRAINT violation Exception"),

	// Response errors for user [start Range: 4000]
	RMS_PARAMETERS_WRONG(4002, "Wrong parameters requested"),
	RMS_MAXIMUM_ATTEMPT_REACHED(4003, "Maximumm number of request attempt reached"),
	RMS_MANDATORY_PARAMETERS_NOT_SET(4004, "Mandatory Parameters missing"), RMS_USER_BLOCKED(4005, "User is blocked"),
	RMS_DATA_NOT_FOUND(4006, "Requested data not found"), RMS_SIZE_VIOLATION(4007, "Size constrain violation"),
	RMS_POLICY_VIOLATION(4008, "Logical policy violation"),

	// Response errors for panel [start Range: 4051]
	RMS_IS_MACHINE(4051, "isMachin is empty or 0"),

	// Response errors for security and policy violation [start Range: 5000]
	RMS_SECURITY_POLICY_VIOLATION(5001, "Security policy violation"), RMS_AUTH_FAILED(4265, "Authentication failed"),
	RMS_DATA_EXIST(4266, "Data Exist"),
	RMS_PAYMENT_DUE(4300, "please pay your charge(amount) in order to use our service"),

	// Custom error codes as per the project

	// User related error code
	RMS_UI_USER_CREATE_SUCCESSFULL(0, "User create successfull."), RMS_UI_USER_CREATE_FAIL(1, "User creation failed."),
	RMS_UI_NOT_VALID_ROLE(2, "Invalid Role."), RMS_UI_USER_IS_ALREADY_REGISTER(4, "User is already registered."),
	RMS_UI_PASSWORD_NOT_MATCH(5, "You entered wrong password."),
	RMS_UI_USER_NOT_VERIFIED(6, "Please check your email for link to create password."),
	RMS_UI_VERIFICATION_EXPIRED(7, "Link has expired."), RMS_AUTH_SUCCESSFULL(0, "User login successfully."),
	// sequence related error code
	RMS_SEQ_ACTION_UNSUPPORTED(1, "Sequence is not generated.");

	int errorCode;
	String description;

	/**
	 * Instantiates a new error codes.
	 * 
	 * @param errorCode   the error code
	 * @param description the description
	 */
	private ErrorCodes(int errorCode, String description) {
		this.errorCode = errorCode;
		this.description = description;
	}

	/**
	 * Gets the error code.
	 * 
	 * @return the error code
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 * 
	 * @param errorCode the new error code
	 */

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

}