package com.industrial.ro.brita.common.utils;

/**
 * @author vishal.p
 *
 */
public final class BritaConstants {

	public static final String JWT_TOKEN_CREATE_TIME = "jwtTokenCreateTime";
	public static final String JWT_TOKEN_ROLE = "userRole";
	public static final String JWT_TOKEN_USER_ID = "userId";
	public static final String JWT_TOKEN_USER_NAME = "userName";
	public static final String SECRET = "abcd1234!@#";
	public static final String UNAUTHORIZED_JSON = "{ \"code\": " + 410 + " }";
	public static final String APPLICATION_JSON_UTF8_VALUE = "application/json;charset=UTF-8";
	public static final String RELAM_NAME = "DeveloperStack";
	public static final String LOGIN_URL = "/authenticate";
	public static final String HEADER_STRING = "Authorization";
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String JWT_TOKEN_DEVICE_TOKEN = "deviceToken";
	public static final String FIREBASE_SERVER_ID = "";
	public static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
	public static final String TOPIC = "gepurifier-1a0a0.firebaseapp.com";
	public static final String FIREBASE_SERVER_KEY = "AAAA5eKJf1c:APA91bHgI_YaLSxWZ7-ewXA4k3lnB6dsrrRSzsJRoMVaTMDQJWoQgMo1OHnf_BTP5DQyhz5E3yP5t-0_HHJECOSrg1Yagv4WIRkfQc7jynVz6GKDI1jg-uhwaGwPM4agiZ5R0pN89g3s";
	public static final String INDEX_HTML = "index.html";
	public static final String COLON = ":";
	public static final String NOT_FOUND = "notFound";
	public static final String FORWARD = "forward";
	public static final String BACKSLASH = "/";
	public static final String FORWARD_INDEX_HTML = FORWARD + COLON + BACKSLASH + INDEX_HTML;
	public static final String BACKSLASH_NOT_FOUND = BACKSLASH + NOT_FOUND;
	public static final String KEY_EQUALS = "key=";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String MIME_MEDIA_TYPE = "application/json";
	public static final String STRING_ZERO = "0";
	public static final String STRING_ONE = "1";

}
