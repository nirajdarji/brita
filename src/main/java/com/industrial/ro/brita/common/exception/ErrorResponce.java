package com.industrial.ro.brita.common.exception;

public class ErrorResponce {

	private int code;

	private String message;

	public ErrorResponce() {
	}

	public ErrorResponce(String message) {
		this.message = message;
	}

	public ErrorResponce(int code, String message) {
		this.message = message;
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
