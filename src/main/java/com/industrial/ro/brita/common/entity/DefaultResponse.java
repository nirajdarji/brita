package com.industrial.ro.brita.common.entity;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

//import com.bms11.aw.dto.Page;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * @author vishal.p
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(NON_NULL)
public class DefaultResponse implements Serializable {

	private static final long serialVersionUID = 2142634337622743221L;

	private Integer code;
	private Map<String, Object> data;
	private Long page;
	private Long pageSize;
	private Long totalRecords;
	private Object[] searchAfter;
	private String message;

	@JsonIgnore
	private HttpStatus httpStatus;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public DefaultResponse() {
	}

	public DefaultResponse(Integer messageStatus) {
		super();
		this.code = messageStatus;
	}

	public DefaultResponse(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public DefaultResponse(Integer code, HttpStatus httpStatus) {
		super();
		this.code = code;
		this.httpStatus = httpStatus;
	}

	public DefaultResponse(Integer messageStatus, Map<String, Object> data) {
		super();
		this.code = messageStatus;
		this.data = data;
	}

	public DefaultResponse(Integer messageStatus, String key, Object responseObject) {
		super();
		this.code = messageStatus;
		this.addData(key, responseObject);
	}

	/*
	 * public DefaultResponse(Integer messageStatus, String key, Object
	 * responseObject, Page page) { this(messageStatus, key, responseObject);
	 * this.page = page.getPageNo(); this.pageSize = page.getPageSize();
	 * this.totalRecords = page.getTotalRecords(); }
	 */
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public final void addData(String key, Object responseObject) {
		if (Objects.isNull(this.data)) {
			this.data = new HashMap<>();
		}
		this.data.put(key, responseObject);
	}

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public Long getPageSize() {
		return pageSize;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public Long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public Object[] getSearchAfter() {
		return searchAfter;
	}

	public void setSearchAfter(Object[] searchAfter) {
		this.searchAfter = searchAfter;
	}

	public DefaultResponse withData(String key, Object data) {
		this.addData(key, data);
		return this;
	}

	public DefaultResponse withMessage(String key, HttpStatus code) {
		// this.addData(key, data);
		this.message = key;
		return this;
	}

	/*
	 * public DefaultResponse withPage(Page page) { this.page = page.getPageNo();
	 * this.pageSize = page.getPageSize(); this.totalRecords =
	 * page.getTotalRecords(); this.searchAfter = page.getSearchAfter(); return
	 * this; }
	 */

	public DefaultResponse withHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
		return this;
	}

	public ResponseEntity<DefaultResponse> build() {
		return new ResponseEntity<>(this, this.httpStatus);
	}

	@Override
	public String toString() {
		return "DefaultResponse [code=" + code + ", data=" + data + (page != null ? ", page=" + page : "")
				+ (pageSize != null ? ", pageSize=" + pageSize : "")
				+ (totalRecords != null ? ", totalRecords=" + totalRecords : "")
				+ (searchAfter != null ? ", searchAfter=" + searchAfter : "") + "]";
	}
}
