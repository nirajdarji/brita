package com.industrial.ro.brita.common.entity;
public enum ResponseMessageCodes {

    SUCCESS(2000),
    CREATED(2001),
    BAD_REQUEST(4000),
    CONFLICT(4009),

    /*
     * Raise this code when we can not perform given operation due to entity's dependency.
     */
    DEPENDENCY_VIOLATION(40091),
    UNAUTHORIZED(4010),
    FORBIDDEN(4030),
    NO_DATA(4040),
    STALE_DATA(4260),
    INTERNAL_SERVER_ERROR(5000),
    EMAIL_NOT_FOUND(4041),
    EXPECTATION_FAILED(4170),
    NO_DATA_IN_FILE(4042);

    private int code;

    ResponseMessageCodes(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}