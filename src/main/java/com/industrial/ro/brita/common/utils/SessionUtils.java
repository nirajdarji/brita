package com.industrial.ro.brita.common.utils;

import org.springframework.security.core.context.SecurityContextHolder;

import com.industrial.ro.brita.entity.UserAuthentication;

/**
 * @author vishal.p
 *
 */
public final class SessionUtils {

	private SessionUtils() {

	}

	public static UserAuthentication getCurrentUser() {
		return (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
	}

	public static String getCurrentUserName() {
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		String userName = null;
		if (name != null) {
			userName = name;
		}
		return userName;
	}

}
