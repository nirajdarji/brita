package com.industrial.ro.brita.service.impl;

import static com.industrial.ro.brita.common.utils.CommonUtils.isNullEmpty;
import static com.industrial.ro.brita.common.utils.SessionUtils.getCurrentUserName;
import static com.industrial.ro.brita.entity.ROLE.ADMIN;
import static com.industrial.ro.brita.entity.ROLE.EMPLOYEE;
import static com.industrial.ro.brita.entity.ROLE.SUPPLIER;
import static java.util.Objects.isNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.industrial.ro.brita.common.exception.CustomException;
import com.industrial.ro.brita.common.exception.ErrorCodes;
import com.industrial.ro.brita.entity.LogInResponse;
import com.industrial.ro.brita.entity.PanelMst;
import com.industrial.ro.brita.entity.UserInformation;
import com.industrial.ro.brita.entity.UserToken;
import com.industrial.ro.brita.repository.PanelMstRepository;
import com.industrial.ro.brita.repository.UserRepository;
import com.industrial.ro.brita.repository.UserTokenRepository;
import com.industrial.ro.brita.service.IUserService;

/**
 * @author vishal.p
 *
 */
@Service
public class UserServiceImpl implements IUserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserTokenRepository userTokenRepository;

	@Autowired
	PanelMstRepository panelRepository;

	@Override
	public String saveOrUpdateOrder(UserInformation userInformation) throws CustomException {
		LOGGER.info("Save or update UserInformation service Called!!");
		try {
			UserInformation userInfo = userRepository.findByUserName(getCurrentUserName());
			String userRole = userInfo.getUserRole();
			String currentUserId = userInfo.getUserId();
			userInformation.setCreator(currentUserId);

			if (userRole.equals(ADMIN.name())) {
				userInformation.setUserRole(SUPPLIER.name());
				List<String> panelIds = new ArrayList<>();
				if (!isNull(userInformation.getPanelIds())) {
					panelIds = new ArrayList<>(userInformation.getPanelIds());
				}
				String supplierId = userInformation.getUserId();
				updatePanelInfo(panelIds, supplierId);
			} else if (userRole.equals(SUPPLIER.name())) {
				userInformation.setUserRole(EMPLOYEE.name());
			} else {
				LOGGER.info("role has no rights to create user. !!");
				throw new CustomException(ErrorCodes.RMS_UI_USER_CREATE_FAIL, "User creation failed.");
			}

			userRepository.save(userInformation);
			LOGGER.info("Save or update UserInformation service Completed!!");
			return "success";
		} catch (DuplicateKeyException d) {
			throw new CustomException(ErrorCodes.RMS_UI_USER_IS_ALREADY_REGISTER, "User is already registered.");
		}

		catch (Exception e) {
			throw new CustomException(ErrorCodes.RMS_INTERNAL,
					"There is some internal issue. Please contact Administrator.");
		}
	}

	@Override
	public LogInResponse authenticate(UserInformation userInformation) throws CustomException {
		LOGGER.info("Login service Called!!");

		String userName = userInformation.getUserName();

		UserInformation userInfo = userRepository.findByUserName(userName);

		validateUserNameAndPassword(userInformation, userName, userInfo);

		String userId = userInfo.getUserId();
		String role = userInfo.getUserRole();
		String deviceToken = userInformation.getDeviceToken();

		UserToken userToken = userTokenRepository.findByUserIdAndDeviceToken(userId, deviceToken);
		if (isNull(userToken)) {
			// set created time
			userTokenRepository.save(new UserToken(userId, deviceToken));
		} else {
			// set update time
			userTokenRepository.save(userToken);
		}
		LOGGER.info("Login service Completed!!");
		return new LogInResponse(userId, userName, role, userInfo.getPanelIds());
	}

	@Override
	public void logout(UserInformation userInformation) throws CustomException {
		if (isNull(userInformation.getDeviceToken())) {
			LOGGER.info("Device Token Not found!!");
			throw new CustomException(ErrorCodes.RMS_UI_NOT_VALID_ROLE, "Device token should not be empty!!");
		}
		userTokenRepository.deleteByDeviceToken(userInformation.getDeviceToken());
		LOGGER.info("successfully logout!!");
	}

	@SuppressWarnings("squid:S1854")
	@Override
	public List<LogInResponse> getAllUser() throws CustomException {
		LOGGER.info("getAllUser service Called!!");
		UserInformation userInfo = userRepository.findByUserName(getCurrentUserName());
		String userRole = userInfo.getUserRole();
		String userId = userInfo.getUserId();

		List<UserInformation> userInformations = new ArrayList<>();
		if (userRole.equals(ADMIN.name())) {
			userInformations = userRepository.findByUserRole(SUPPLIER.name());
		} else if (userRole.equals(SUPPLIER.name())) {
			userInformations = userRepository.findByUserRoleAndCreator(EMPLOYEE.name(), userId);
		} else {
			LOGGER.info("role has no rights to get user!!");
			throw new CustomException(ErrorCodes.RMS_UI_NOT_VALID_ROLE, "role has no rights to get user");
		}
		List<LogInResponse> logInResponses = new ArrayList<>();
		for (UserInformation userInformation : userInformations) {
			logInResponses.add(new LogInResponse(userInformation.getUserId(), userInformation.getUserName(),
					userInformation.getUserEmail(), userInformation.getUserRole(), null,
					userInformation.getPanelIds()));
		}
		LOGGER.info("getAllUser service Completed!!");
		return logInResponses;
	}

	@Override
	public UserInformation getUserById(String userId) throws CustomException {
		LOGGER.info("getUserById service Called!!");
		Optional<UserInformation> userInformation = userRepository.findById(userId);
		if (!userInformation.isPresent()) {
			throw new CustomException(ErrorCodes.RMS_DATA_EXIST, "Requested data not find");
		}
		LOGGER.info("getUserById service Completed!!");
		return userInformation.get();
	}
	
	@Override
	public boolean deleteUserById(String userId) throws CustomException {
		LOGGER.info("Delete user by id service Called!! userId :: '{}'", userId);
		boolean isUserExisted = userRepository.existsById(userId);
		if (!isUserExisted) {
			throw new CustomException(ErrorCodes.RMS_DATA_NOT_FOUND, "User not found !!");
		}
		userRepository.deleteById(userId);
		LOGGER.info("Delete user by id service Completed!! userId :: '{}'", userId);
		return isUserExisted;
	}

	@SuppressWarnings({ "squid:S3655", "squid:S2259" })
	private void updatePanelInfo(List<String> panelIds, String supplierId) {
		if (!isNull(supplierId)) {
			Optional<UserInformation> supplierInformation = userRepository.findById(supplierId);
			List<String> existingPanelIds = supplierInformation.get().getPanelIds();

			if (!isNull(existingPanelIds)) {
				List<String> removePanelIds = new ArrayList<>(existingPanelIds);
				if (!isNull(panelIds)) {
					removePanelIds.removeAll(panelIds);
				}
				if (!isNullEmpty(removePanelIds)) {
					updatePanelIsAllocated(removePanelIds, false);
				}

			}
			if (!isNullEmpty(panelIds)) {
				if (!isNull(existingPanelIds)) {
					panelIds.removeAll(existingPanelIds);
				}

				updatePanelIsAllocated(panelIds, true);
			}
		} else {
			updatePanelIsAllocated(panelIds, true);

		}
	}

	private void validateUserNameAndPassword(UserInformation userInformation, String userName, UserInformation userInfo)
			throws CustomException {
		if (isNull(userInfo) || !userName.equals(userInfo.getUserName())) {
			LOGGER.info("Username not Matched !!");
			throw new CustomException(ErrorCodes.RMS_UI_USER_NOT_VERIFIED,
					"Please check your email for link to create password.");
		}
		String password = userInformation.getUserPassword();
		if (!password.equals(userInfo.getUserPassword())) {
			LOGGER.info("Password not Matched !!");
			throw new CustomException(ErrorCodes.RMS_UI_PASSWORD_NOT_MATCH, "You entered wrong password.");
		}
	}

	@SuppressWarnings("squid:S3655")
	private void updatePanelIsAllocated(List<String> panelIds, boolean isAllocated) {
		if (!isNull(panelIds)) {
			for (String panelId : panelIds) {
				Optional<PanelMst> panelMst = panelRepository.findById(panelId);
				panelMst.get().setIsAllocated(isAllocated);
				panelRepository.save(panelMst.get());
			}
		}

	}

}
