package com.industrial.ro.brita.service.impl;

import static com.industrial.ro.brita.common.utils.BritaConstants.CONTENT_TYPE;
import static com.industrial.ro.brita.common.utils.BritaConstants.FIREBASE_API_URL;
import static com.industrial.ro.brita.common.utils.BritaConstants.FIREBASE_SERVER_KEY;
import static com.industrial.ro.brita.common.utils.BritaConstants.HEADER_STRING;
import static com.industrial.ro.brita.common.utils.BritaConstants.KEY_EQUALS;
import static com.industrial.ro.brita.common.utils.BritaConstants.MIME_MEDIA_TYPE;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.industrial.ro.brita.entity.HeaderRequestInterceptor;

@Service
public class AndroidPushNotificationsServiceImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(AndroidPushNotificationsServiceImpl.class);

	@Async
	public CompletableFuture<String> send(HttpEntity<String> entity) {

		RestTemplate restTemplate = new RestTemplate();
		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new HeaderRequestInterceptor(HEADER_STRING, KEY_EQUALS + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor(CONTENT_TYPE, MIME_MEDIA_TYPE));
		restTemplate.setInterceptors(interceptors);
		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);
		LOGGER.info("firebaseResponse :: '{}'", firebaseResponse);
		return CompletableFuture.completedFuture(firebaseResponse);
	}
}
