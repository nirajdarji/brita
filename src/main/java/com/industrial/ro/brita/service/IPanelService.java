package com.industrial.ro.brita.service;

import java.util.List;

import com.industrial.ro.brita.common.exception.CustomException;
import com.industrial.ro.brita.entity.PanelFilters;
import com.industrial.ro.brita.entity.PanelInfo;
import com.industrial.ro.brita.entity.PanelMst;
import com.industrial.ro.brita.entity.dto.PanelDTO;

public interface IPanelService {

	String saveOrUpdateOrder(PanelMst panelMst) throws CustomException;

	List<PanelDTO> getAllPanel(PanelFilters panelFilters) throws CustomException;

	PanelInfo panelById(String panelId) throws CustomException;

	boolean deletePanelById(String panelId) throws CustomException;
}
