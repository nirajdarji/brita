package com.industrial.ro.brita.service;

import java.util.List;

import com.industrial.ro.brita.common.exception.CustomException;
import com.industrial.ro.brita.entity.LogInResponse;
import com.industrial.ro.brita.entity.UserInformation;

/**
 * @author vishal.p
 *
 */
public interface IUserService {

	String saveOrUpdateOrder(UserInformation userInformation) throws CustomException;

	LogInResponse authenticate(UserInformation userInformation) throws CustomException;

	void logout(UserInformation userInformation) throws CustomException;

	List<LogInResponse> getAllUser() throws CustomException;

	UserInformation getUserById(String userId) throws CustomException;

	boolean deleteUserById(String userId) throws CustomException;

}
