package com.industrial.ro.brita.service.impl;

import static com.industrial.ro.brita.common.utils.BritaConstants.STRING_ONE;
import static com.industrial.ro.brita.common.utils.BritaConstants.STRING_ZERO;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static com.industrial.ro.brita.common.utils.CommonUtils.isNullEmpty;
import static com.industrial.ro.brita.common.utils.SessionUtils.getCurrentUserName;
import static com.industrial.ro.brita.entity.ROLE.ADMIN;
import static com.industrial.ro.brita.entity.ROLE.EMPLOYEE;
import static com.industrial.ro.brita.entity.ROLE.SUPPLIER;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;

import com.industrial.ro.brita.common.exception.CustomException;
import com.industrial.ro.brita.common.exception.ErrorCodes;
import com.industrial.ro.brita.entity.LogInResponse;
import com.industrial.ro.brita.entity.PanelFilters;
import com.industrial.ro.brita.entity.PanelInfo;
import com.industrial.ro.brita.entity.PanelMst;
import com.industrial.ro.brita.entity.UserInformation;
import com.industrial.ro.brita.entity.UserToken;
import com.industrial.ro.brita.entity.dto.PanelDTO;
import com.industrial.ro.brita.repository.PanelMstRepository;
import com.industrial.ro.brita.repository.UserRepository;
import com.industrial.ro.brita.repository.UserTokenRepository;
import com.industrial.ro.brita.service.IPanelService;

@Service
public class PanelServiceImpl implements IPanelService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PanelServiceImpl.class);

	@Autowired
	PanelMstRepository panelRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserTokenRepository userTokenRepository;

	@Autowired
	AndroidPushNotificationsServiceImpl androidPushNotificationsServiceImpl;

	@Override
	public String saveOrUpdateOrder(PanelMst panelObject) throws CustomException {
		
		LOGGER.info("Save or update Panel service Called!!");
		if(isBlank(panelObject.getPid()) || (!isBlank(panelObject.getIm()) && panelObject.getIm().equals(STRING_ZERO))) {
			return addPanel(panelObject);
		}
		return updatePanel(panelObject);
	}
	
	private String addPanel(PanelMst panelObject) throws CustomException {
		if (isBlank(panelObject.getIm()) || !panelObject.getIm().equals(STRING_ZERO)) {
			LOGGER.info("Not able to save or update panel because isMachin is empty or not 0.!!");
			throw new CustomException(ErrorCodes.RMS_IS_MACHINE,
					"Not able to save or update panel because isMachin is empty or not 0.");
		}
		return addOrUpdatePanels(panelObject);
	}
	
	private String updatePanel(PanelMst panelObject) throws CustomException {
		if (isBlank(panelObject.getIm()) || !panelObject.getIm().equals(STRING_ONE)) {
			LOGGER.info("Not able to save or update panel because isMachin is empty or not 1.!!");
			throw new CustomException(ErrorCodes.RMS_IS_MACHINE,
					"Not able to save or update panel because isMachin is empty or not 1.");
		}
		boolean isPanelExists = panelRepository.existsById(panelObject.getPid());
		if(!isPanelExists) {
			throw new CustomException(ErrorCodes.RMS_DATA_NOT_FOUND, "Panel not found !!");
		}
		return addOrUpdatePanels(panelObject);
	}

	private String addOrUpdatePanels(PanelMst panelObject) throws CustomException {
		UserInformation userInfo = userRepository.findByUserName(getCurrentUserName());
		String userRole = userInfo.getUserRole();
		String userId = userInfo.getUserId();
		String panelMstId = panelObject.getPid();
		if (userRole.equals(EMPLOYEE.name())) {
			LOGGER.info("role has no rights to save or update panel!!");
			throw new CustomException(ErrorCodes.RMS_INTERNAL, "User has no rights to save or update panel.");
		}

		if (userRole.equals(SUPPLIER.name()) && isNull(panelMstId)) {
			LOGGER.info("role has no rights to create panel!!");
			throw new CustomException(ErrorCodes.RMS_INTERNAL, "User has no rights to create panel.");

		}
		if (isNull(panelObject.getIsAllocated())) {
			panelObject.setIsAllocated(false);
		}
		panelRepository.save(panelObject);

		if (!isNull(panelMstId)) {
			if (userRole.equals(ADMIN.name())) {
				UserInformation supplierInfo = userRepository.findByPanelIds(panelMstId);
				if (!isNull(supplierInfo)) {
					pushNotification(panelObject, supplierInfo.getUserId());
				}

			} else {
				pushNotification(panelObject, userId);
			}

		}
		LOGGER.info("Save or update Panel service Completed!!");
		return "success";
	}

	@SuppressWarnings("squid:S3655")
	@Override
	public List<PanelDTO> getAllPanel(PanelFilters panelFilters) throws CustomException {
		LOGGER.info("Getall Panels service Called!!");
		getCurrentUserName();
		UserInformation userInfo = userRepository.findByUserName(getCurrentUserName());
		String userRole = userInfo.getUserRole();
		String userId = userInfo.getUserId();
		List<PanelMst> panelMstList = new ArrayList<>();
		Boolean isAllocated = panelFilters.getIsAllocated();

		if (userRole.equals(ADMIN.name())) {
			if (isNull(isAllocated)) {
				panelMstList = panelRepository.findAll();
			} else {
				panelMstList = panelRepository.findByIsAllocated(isAllocated);
			}

		} else if (userRole.equals(SUPPLIER.name())) {
			Optional<UserInformation> supplierInformation = userRepository.findById(userId);
			List<String> panelIds = supplierInformation.get().getPanelIds();
			if (!isNullEmpty(panelIds)) {
				panelMstList = (List<PanelMst>) panelRepository.findAllById(panelIds);
			}

		} else {

			Optional<UserInformation> employeeInformation = userRepository.findById(userId);
			String supplierId = employeeInformation.get().getCreator();
			Optional<UserInformation> supplierInformation = userRepository.findById(supplierId);
			List<String> panelIds = supplierInformation.get().getPanelIds();
			if (!isNullEmpty(panelIds)) {
				panelMstList = (List<PanelMst>) panelRepository.findAllById(panelIds);
			}
		}

		if (panelMstList == null || panelMstList.isEmpty())
			throw new CustomException(ErrorCodes.RMS_DATA_NOT_FOUND, "There is no panel for this user.");
		else {
			Iterator<PanelMst> panelMsItr = panelMstList.iterator();
			List<PanelDTO> panelDtoList = new ArrayList<>();
			while (panelMsItr.hasNext()) {
				PanelMst panelMst = panelMsItr.next();
				PanelDTO panelDto = new PanelDTO();
				panelDto.setPi(panelMst.getPid());
				panelDto.setPn(panelMst.getPn());

				panelDtoList.add(panelDto);
			}
			LOGGER.info("Getall Panels service Completed!!");
			return panelDtoList;
		}
	}

	@Override
	public PanelInfo panelById(String panelId) throws CustomException {
		LOGGER.info("Get panel by id service Called!! panelId :: '{}'", panelId);
		Optional<PanelMst> panelMstOt = panelRepository.findById(panelId);
		if (!panelMstOt.isPresent()) {
			throw new CustomException(ErrorCodes.RMS_DATA_EXIST, "Requested data not find");
		}

		UserInformation supplierInformation = userRepository.findByPanelIds(panelId);

		LogInResponse userInfo = new LogInResponse();
		if (isNull(supplierInformation)) {
			LOGGER.info("Get panel by id service Completed!! panelId :: '{}'", panelId);
			return new PanelInfo(panelMstOt.get(), userInfo);

		}
		userInfo = new LogInResponse(supplierInformation.getUserId(), supplierInformation.getUserName(),
				supplierInformation.getUserRole(), null, supplierInformation.getPanelIds());

		LOGGER.info("Get panel by id service Completed!! panelId :: '{}'", panelId);
		return new PanelInfo(panelMstOt.get(), userInfo);
	}

	@Override
	public boolean deletePanelById(String panelId) throws CustomException {
		LOGGER.info("Delete panel by id service Called!! panelId :: '{}'", panelId);
		boolean isPanelExisted = panelRepository.existsById(panelId);
		if (!isPanelExisted) {
			throw new CustomException(ErrorCodes.RMS_DATA_NOT_FOUND, "Panel not found !!");
		}
		panelRepository.deleteById(panelId);
		LOGGER.info("Delete panel by id service Completed!! panelId :: '{}'", panelId);
		return isPanelExisted;
	}

	private void pushNotification(PanelMst panelObject, String supplierId) throws CustomException {

		List<UserInformation> employeeInformations = new ArrayList<>();
		if (!isNull(supplierId)) {
			employeeInformations = userRepository.findByCreator(supplierId);
		}

		if (!isNullEmpty(employeeInformations)) {
			List<String> employeeIds = employeeInformations.stream().map(UserInformation::getUserId).collect(toList());

			if (!isNullEmpty(employeeIds)) {
				List<UserToken> employeeTokens = userTokenRepository.findByUserIdIn(employeeIds);

				List<String> employeeDeviceTokens = employeeTokens.stream().map(UserToken::getDeviceToken)
						.collect(toList());
				employeeDeviceTokens.remove(null);
				if (!isNullEmpty(employeeDeviceTokens)) {
					JSONObject body = preparedNotificationBody(panelObject, employeeDeviceTokens);
					HttpEntity<String> request = new HttpEntity<>(body.toString());
					androidPushNotificationsServiceImpl.send(request);
				}

			}
		}

	}

	private JSONObject preparedNotificationBody(PanelMst panelObject, List<String> deviceTokens)
			throws CustomException {
		try {
			LOGGER.info("Send Notification Using  Device Tokens :: {}", deviceTokens);
			JSONObject body = new JSONObject();
			body.put("registration_ids", new JSONArray(deviceTokens));
			body.put("priority", "high");

			JSONObject notification = new JSONObject();
			notification.put("title", "Brita");
			notification.put("body", "panel");

			JSONObject data = new JSONObject();
			data.put("Key-1", panelObject);

			body.put("notification", notification);
			body.put("data", data);
			return body;
		} catch (JSONException e) {
			throw new CustomException(ErrorCodes.RMS_INTERNAL, e.getMessage());
		}
	}

}
