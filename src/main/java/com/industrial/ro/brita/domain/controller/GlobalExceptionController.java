package com.industrial.ro.brita.domain.controller;

import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.industrial.ro.brita.common.exception.CustomException;
import com.industrial.ro.brita.common.exception.ErrorResponce;

@ControllerAdvice
public class GlobalExceptionController {

	@ExceptionHandler(ServletRequestBindingException.class)
	public ResponseEntity<ErrorResponce> exceptionHandler(ServletRequestBindingException ex) {
		ErrorResponce error = new ErrorResponce();
		error.setCode(HttpStatus.FORBIDDEN.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.OK);
	}

	@ExceptionHandler(CustomException.class)
	public ResponseEntity<ErrorResponce> exceptionHandler(CustomException ex) {
		ErrorResponce error = new ErrorResponce();
		error.setCode(ex.getErrorCode());
		error.setMessage(ex.getErrorMessage());
		return new ResponseEntity<>(error, HttpStatus.OK);
	}

	@ExceptionHandler(JSONException.class)
	public ResponseEntity<ErrorResponce> exceptionHandler(JSONException ex) {
		ErrorResponce error = new ErrorResponce();
		error.setCode(HttpStatus.BAD_REQUEST.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.OK);
	}

}
