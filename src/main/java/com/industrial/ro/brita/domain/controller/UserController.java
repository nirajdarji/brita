package com.industrial.ro.brita.domain.controller;

import static com.industrial.ro.brita.common.utils.DefaultResponseBuilder.success;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.industrial.ro.brita.common.entity.DefaultResponse;
import com.industrial.ro.brita.common.exception.CustomException;
import com.industrial.ro.brita.entity.LogInResponse;
import com.industrial.ro.brita.entity.UserInformation;
import com.industrial.ro.brita.service.IUserService;

/**
 * @author vishal.p
 *
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@Autowired
	IUserService userServiceImpl;

	@PostMapping(value = "/saveorupdateuser")
	public ResponseEntity<DefaultResponse> saveOrUpdatePannel(@RequestBody UserInformation userInformation)
			throws CustomException {
		LOGGER.info("Save or update UserInformation !!");
		userServiceImpl.saveOrUpdateOrder(userInformation);
		return success().withMessage("Success", HttpStatus.OK).build();
	}

	@PostMapping(value = "/authenticate")
	public ResponseEntity<UserInformation> authenticate(@RequestBody UserInformation userInformation)
			throws CustomException {
		LOGGER.info("Login request start !!");
		return new ResponseEntity(userServiceImpl.authenticate(userInformation), HttpStatus.OK);

	}

	@PostMapping(path = "/devicelogout")
	public ResponseEntity<DefaultResponse> logout(@RequestBody UserInformation userInformation) throws CustomException {
		LOGGER.info("Logout request start !!");
		userServiceImpl.logout(userInformation);
		return success().withMessage("Logout Success", HttpStatus.OK).build();
	}

	@GetMapping(value = "/users")
	public ResponseEntity<List<LogInResponse>> getAllUser() throws CustomException {
		LOGGER.info("Get all users!!");
		return new ResponseEntity<>(userServiceImpl.getAllUser(), HttpStatus.OK);
	}

	@GetMapping(value = "/userbyid")
	public ResponseEntity<UserInformation> getUserById(@RequestParam String userId) throws CustomException {
		LOGGER.info("Get  users by id!!");
		return new ResponseEntity<>(userServiceImpl.getUserById(userId), HttpStatus.OK);
	}

	@DeleteMapping(value = "/userbyid")
	public ResponseEntity<DefaultResponse> deleteUserById(@RequestParam String userId) throws CustomException {
		LOGGER.info("Delete User By Id service called. Id {} is.", userId);
		userServiceImpl.deleteUserById(userId);
		return success().withMessage("Delete Successfully !!", HttpStatus.OK).build();
	}

}
