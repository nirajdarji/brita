package com.industrial.ro.brita.domain.controller;

import static com.industrial.ro.brita.common.utils.DefaultResponseBuilder.success;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.industrial.ro.brita.common.entity.DefaultResponse;
import com.industrial.ro.brita.common.exception.CustomException;
import com.industrial.ro.brita.entity.PanelFilters;
import com.industrial.ro.brita.entity.PanelInfo;
import com.industrial.ro.brita.entity.PanelMst;
import com.industrial.ro.brita.entity.dto.PanelDTO;
import com.industrial.ro.brita.service.IPanelService;

/**
 * @author vishal
 *
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class PanelController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PanelController.class);

	@Autowired
	IPanelService panelServiceImpl;

	@PostMapping(value = "/saveorupdatepanel")
	public ResponseEntity<DefaultResponse> saveOrUpdatePannel(@RequestBody PanelMst panelMst) throws CustomException {
		LOGGER.info("Save or update PanelMst!!");
		panelServiceImpl.saveOrUpdateOrder(panelMst);
		return success().withMessage("Success", HttpStatus.OK).build();
	}

	@GetMapping(value = "/panels")
	public ResponseEntity<List<PanelDTO>> panels(
			@RequestParam(name = "isAllocated", required = false) Boolean isAllocated) throws CustomException {
		LOGGER.info("Get all panels!!");
		PanelFilters panelFilters = new PanelFilters(isAllocated);
		return new ResponseEntity<>(panelServiceImpl.getAllPanel(panelFilters), HttpStatus.OK);
	}

	@GetMapping(value = "/panelbyid")
	public ResponseEntity<PanelInfo> panelById(@RequestParam String pid) throws CustomException {

		LOGGER.info("Get By Id service called. Id {} is.", pid);
		return new ResponseEntity<>(panelServiceImpl.panelById(pid), HttpStatus.OK);
	}

	@DeleteMapping(value = "/panelbyid")
	public ResponseEntity<DefaultResponse> deletePanelById(@RequestParam String pid) throws CustomException {
		LOGGER.info("Delete By Id service called. Id {} is.", pid);
		panelServiceImpl.deletePanelById(pid);
		return success().withMessage("Delete Successfully !!", HttpStatus.OK).build();
	}
}
