export const LocalStorage = {
    UserData: 'USER_DATA',
    Token: 'TOKEN'
};

export const UserRole = {
    admin: 'ADMIN',
    supplier: 'SUPPLIER',
    employee: 'EMPLOYEE'
};

export const Api = {
    Authenticate: 'authenticate',
    Logout: 'devicelogout',
    Panels: 'panels',
    PanelById: 'panelbyid',
    SaveOrUpdatePanel: 'saveorupdatepanel',
    SaveOrUpdateUser: 'saveorupdateuser',
    GetUsers: 'users',
    GetUserById: 'userbyid',
};

export const Validation = {
    login: {
        email: 'Opps! Please check once...',
        password: 'Opps! Please check once...'
    }
};

export const FirebaseConfig = {
    apiKey: 'AIzaSyDvmMcKc4Rab5cfTuOTtWne8GQqi2OSptI',
    authDomain: 'gepurifier-1a0a0.firebaseapp.com',
    databaseURL: 'https://gepurifier-1a0a0.firebaseio.com',
    projectId: 'gepurifier-1a0a0',
    storageBucket: 'gepurifier-1a0a0.appspot.com',
    messagingSenderId: '987348172631',
    appId: '1:987348172631:web:0fc0d6bc750f7e41'
};
export const PanelObject = {
    pid: { label: 'Panel Id' },
    pn: { label: 'Panel Name' },
    im: { label: 'Is Machine' },
    pst: { label: 'Panel Status' },
    pop: { label: 'Panel Auto/Manual' },
    ovv: { label: 'Over Voltage Set Point (V)' },
    uvv: { label: 'Under Voltage Set Point (V)' },
    roc: { label: 'Raw Water Pump Overload Current (A)' },
    huc: { label: 'High Pressure Pump Dry Run Current (A)' },
    ruc: { label: 'Raw Water Pump Dry Run Current (A)' },
    hoc: { label: 'High Pressure Pump Overload Current (A)' },
    alt: { label: 'Alert' },
    rwt: { label: 'Raw Water Totalizer (L)' },
    rjt: { label: 'Reject Water Totalizer (L)' },
    pwt: { label: 'Permeate Water Totalizer (L)' },
    rtd: { label: 'Raw Water TDS (PPM)' },
    ptd: { label: 'Permeate Water TDS (PPM)' },
    userName: { label: 'User Name' },
    userRole: { label: 'User Role' }
};
