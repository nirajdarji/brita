import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Api, LocalStorage } from './app.constant';
import { CommonService } from './shared/services/common.service';
import { ApiResponse } from './shared/modals/response';
import { PanelList } from './shared/modals/panel-list';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(
    private http: HttpClient,
    private commonService: CommonService
  ) { }

  // --------------------------------------------------------------------------------------

  userAuthenticate(data): Observable<ApiResponse> {
    const url = `${environment.path}${Api.Authenticate}`;
    return this.http.post<any>(url, data)
      .pipe(
        // tap(data => console.log('Getting Panels Successfully')),
        map(res => {
          if (res.userId && res.userId !== null) {
            return { status: true, message: 'userAuthenticate done', data: res };
          } else {
            return { status: false, message: res.message, data: null };
          }
        }),
        catchError(this.handleError<any>('Error', { status: false, message: 'userAuthenticate Error' }))
      );
  }
  // --------------------------------------------------------------------------------------
  userLogout(): Observable<ApiResponse> {

    const url = `${environment.path}${Api.Logout}`;
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     Authorization: `Bearer ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
    //   })
    // };
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      })
    };
    const data = this.commonService.getLocalStorageObject(LocalStorage.Token);
    return this.http.post<any>(url, { deviceToken: data }, httpOptions)
      .pipe(
        map(res => {
          return { status: true, message: 'userLogout done', data: res };
        }),
        catchError(this.handleError<any>('Error', { status: false, message: 'userLogout Error', data: null }))
      );
  }
  // --------------------------------------------------------------------------------------
  getAllPanels(): Observable<PanelList[] | any> {
    const url = `${environment.path}${Api.Panels}`;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      })
    };
    return this.http.get<any>(url, httpOptions)
      .pipe(
        // map(res => {
        //   return { status: true, message: 'getAllPanels done', data: res };
        // }),
        tap(data => console.log('Getting Panels Successfully')),
        catchError(this.handleError<any>('Error', { status: false, message: 'getAllPanels Error', data: null }))
      );
  }
  // --------------------------------------------------------------------------------------
  getAllNotAllocatedPanels(): Observable<PanelList[]> {
    const url = `${environment.path}${Api.Panels}`;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      }),
      params: { isAllocated: 'false' }
    };
    return this.http.get<any>(url, httpOptions)
      .pipe(
        // map(res => {
        //   return { status: true, message: 'getAllPanels done', data: res };
        // }),
        tap(data => console.log('Getting Panels Successfully')),
        catchError(this.handleError<any>('Error', { status: false, message: 'getAllPanels Error', data: null }))
      );
  }
  // --------------------------------------------------------------------------------------
  getUsers(): Observable<ApiResponse> {
    const url = `${environment.path}${Api.GetUsers}`;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      })
    };
    return this.http.get<any>(url, httpOptions)
      .pipe(
        map(res => {
          return { status: true, message: 'getUsers done', data: res };
        }),
        catchError(this.handleError<any>('Error', { status: false, message: 'getUsers Error', data: null }))
      );
  }
  // --------------------------------------------------------------------------------------
  getPanelDetailById(id): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      }),
      params: { pid: id }
    };
    const url = `${environment.path}${Api.PanelById}`;
    return this.http.get<any>(url, httpOptions)
      .pipe(
        map(res => {
          return { status: true, message: 'getPanelDetailById done', data: res };
        }),
        // tap(data => console.log('Getting Panel Detail Successfully')),
        catchError(this.handleError<any>('Error', { status: false, message: 'getPanelDetailById Error', data: null }))
      );
  }
  // --------------------------------------------------------------------------------------
  getUserDetailById(id): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      }),
      params: { userId: id }
    };
    const url = `${environment.path}${Api.GetUserById}`;
    return this.http.get<any>(url, httpOptions)
      .pipe(
        // map(res => {
        //   return { status: true, message: 'getUserDetailById done', data: res };
        // }),
        tap(data => console.log('Getting User Detail Successfully')),
        catchError(this.handleError<any>('Error', { status: false, message: 'getUserDetailById Error', data: null }))
      );
  }
  // --------------------------------------------------------------------------------------
  removeUserById(id): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      }),
      params: { userId: id }
    };
    const url = `${environment.path}${Api.GetUserById}`;
    return this.http.delete<any>(url, httpOptions)
      .pipe(
        // map(res => {
        //   return { status: true, message: 'getUserDetailById done', data: res };
        // }),
        tap(data => console.log('Removed User Successfully')),
        catchError(this.handleError<any>('Error', { status: false, message: 'removeUserById Error', data: null }))
      );
  }
  // --------------------------------------------------------------------------------------
  addOrUpdatePanel(panelData: any): Observable<ApiResponse> {
    const url = `${environment.path}${Api.SaveOrUpdatePanel}`;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      })
    };
    return this.http.post<any>(url, panelData, httpOptions)
      .pipe(
        map(res => {
          return { status: true, message: 'addOrUpdatePanel done', data: res };
        }),
        catchError(this.handleError<any>('Error', { status: false, message: 'addOrUpdatePanel Error', data: null }))
      );
  }
  // --------------------------------------------------------------------------------------
  removePanelById(id: any): Observable<ApiResponse> {
    const url = `${environment.path}${Api.PanelById}`;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      }),
      params: { pid: id }
    };
    return this.http.delete<any>(url, httpOptions)
      .pipe(
        map(res => {
          return { status: true, message: 'addOrUpdatePanel done', data: res };
        }),
        catchError(this.handleError<any>('Error', { status: false, message: 'addOrUpdatePanel Error', data: null }))
      );
  }
  // --------------------------------------------------------------------------------------
  addOrUpdateUser(userData: any): Observable<ApiResponse> {
    const url = `${environment.path}${Api.SaveOrUpdateUser}`;
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Basic ${this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')}`,
      })
    };
    return this.http.post<any>(url, userData, httpOptions)
      .pipe(
        map(res => {
          return { status: true, message: 'addOrUpdateUser done', data: res };
        }),
        catchError(this.handleError<any>('Error', { status: false, message: 'addOrUpdateUser Error', data: null }))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }
}
