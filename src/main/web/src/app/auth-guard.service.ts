import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { CommonService } from './shared/services/common.service';
import { LocalStorage } from './app.constant';
import { Utilities } from './shared/services/utilities';

@Injectable()
export class AuthGuardService implements CanLoad {

    constructor(private commonService: CommonService, private router: Router) { }

    canLoad() {
        const data = this.commonService.getLocalStorageObject(LocalStorage.UserData);

        if (!Utilities.isEmptyObj(data)) {
            return true;
        } else {
            this.commonService.clearLocalStorageObject(LocalStorage.UserData);
            this.router.navigate(['auth', 'login']);
            return false;
        }
    }
}
