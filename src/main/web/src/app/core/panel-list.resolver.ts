import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { PanelList } from '../shared/modals/panel-list';

@Injectable()
export class PanelListResolver implements Resolve<any> {

    userId: number;

    constructor(private appService: AppService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> {
        return forkJoin([this.appService.getAllPanels(), this.appService.getAllNotAllocatedPanels()]);
    }
}
