import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { UserRoleCondition } from 'src/app/shared/modals/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  panelList: any[];
  role: UserRoleCondition;

  constructor(private appService: AppService, private commonService: CommonService) { }

  ngOnInit() {
    this.panelList = [];
    this.role = this.commonService.checkUserRole();
  }

}
