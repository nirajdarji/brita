import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { ApiResponse } from 'src/app/shared/modals/response';
import { UserRoleCondition } from 'src/app/shared/modals/user';

@Component({
  selector: 'app-panel-list',
  templateUrl: './panel-list.component.html',
  styleUrls: ['./panel-list.component.css']
})
export class PanelListComponent implements OnInit {

  panelList: any[];
  role: UserRoleCondition;

  constructor(private appService: AppService, private commonService: CommonService) { }

  private getAllDataCall() {
    this.appService.getAllPanels().subscribe(response => {
      if (response && response.length > 0) {
        this.panelList = response;
      } else {
        this.commonService.showToaster(response.message, 'warning', 1000);
      }
    });
  }

  ngOnInit() {
    this.role = this.commonService.checkUserRole();
    this.panelList = [];
    this.getAllDataCall();
    // setInterval(() => {
    //   this.getAllDataCall();
    // }, 1000 * 60);
  }

  removePanel(pid) {
    this.appService.removePanelById(pid).subscribe(response => {
      this.panelList = [];
      this.getAllDataCall();
      this.commonService.showToaster('Panel Deleted', 'info', 1000);
    });
  }

}
