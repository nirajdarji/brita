import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { CommonService } from '../shared/services/common.service';
import { AppService } from '../app.service';
import { LocalStorage } from '../app.constant';
import { UserRoleCondition } from '../shared/modals/user';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.css']
})
export class CoreComponent implements OnInit {

  path: string = null;
  loginUserData: any = null;
  role: UserRoleCondition;
  isSidebarOpen: boolean;

  constructor(
    private router: Router,
    private commonService: CommonService,
    private appService: AppService
  ) { }

  private activeSidebar(url) {
    if (url.indexOf('dashboard') > -1) {
      this.path = 'dashboard';
    } else if (url.indexOf('user') > -1) {
      this.path = 'user';
    } else if (url.indexOf('panel') > -1) {
      this.path = 'panel';
    } else {
      this.path = 'dashboard';
    }
  }

  ngOnInit() {
    this.isSidebarOpen = false;
    this.router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        this.activeSidebar(this.router.url);
      }
    });
    this.activeSidebar(this.router.url);
    this.loginUserData = this.commonService.getLocalStorageObject(LocalStorage.UserData);
    this.role = this.commonService.checkUserRole();
  }

  userLogout() {
    this.appService.userLogout().subscribe(response => {
      if (response.status) {
        this.commonService.clearLocalStorageObject(LocalStorage.UserData);
        this.router.navigate(['auth', 'login']);
      }
    });
  }
}
