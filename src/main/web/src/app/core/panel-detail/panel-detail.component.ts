import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { UserRoleCondition } from 'src/app/shared/modals/user';
import { Subscription } from 'rxjs';
import { AppFirebaseService } from 'src/app/app-firebase.service';
import { PanelObject } from 'src/app/app.constant';

@Component({
  selector: 'app-panel-detail',
  templateUrl: './panel-detail.component.html',
  styleUrls: ['./panel-detail.component.css']
})
export class PanelDetailComponent implements OnInit, OnDestroy {

  panelData = [];
  panelDetail = {};
  userDetail = {};
  panelId: string;
  panelForm: FormGroup;
  isEdit: boolean;
  role: UserRoleCondition;
  subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private appService: AppService,
    private commonService: CommonService,
    private firebaseService: AppFirebaseService,
    private formBuilder: FormBuilder) {
  }

  private checkValuesYesNo(element, key) {
    let data = null;
    if (key === 'pop') {
      switch (element) {
        case '1':
          data = 'AUTO';
          break;
        case 1:
          data = 'AUTO';
          break;
        case '0':
          data = 'MANUAL';
          break;
        case 0:
          data = 'MANUAL';
          break;
        default:
          data = element;
          break;
      }
    } else if (key === 'alt') {
      switch (element) {
        case '1':
          data = 'YES';
          break;
        case 1:
          data = 'YES';
          break;
        case '0':
          data = 'NO';
          break;
        case 0:
          data = 'NO';
          break;
        default:
          data = element;
          break;
      }
    } else {
      switch (element) {
        case '1':
          data = 'ON';
          break;
        case 1:
          data = 'ON';
          break;
        case '0':
          data = 'OFF';
          break;
        case 0:
          data = 'OFF';
          break;
        default:
          data = element;
          break;
      }
    }

    return data;
  }

  private getPanelDataById(id) {
    this.appService.getPanelDetailById(id).subscribe(response => {
      this.panelDetail = Object.assign({}, response.data.panel);
      this.userDetail = Object.assign({}, response.data.user);
      this.setValue();
      this.panelData = [];

      for (const key in this.panelDetail) {
        if (this.panelDetail.hasOwnProperty(key)) {
          if (this.role.isAdmin) {
            if (key !== 'im' && key !== 'isAllocated') {
              const element = this.panelDetail[key];
              this.panelData.push({
                label: PanelObject[key].label,
                value: this.checkValuesYesNo(element, key)
              });
            }
          } else {
            if (key !== 'pid' && key !== 'im' && key !== 'isAllocated') {
              const element = this.panelDetail[key];
              this.panelData.push({
                label: PanelObject[key].label,
                value: this.checkValuesYesNo(element, key)
              });
            }
          }
        }
      }
      for (const key in this.userDetail) {
        if (this.userDetail.hasOwnProperty(key)) {
          if (this.role.isAdmin) {
            if (key !== 'pid' && key !== 'panelIds' && key !== 'userId') {
              const element = this.userDetail[key];
              this.panelData.push({
                label: PanelObject[key].label,
                value: element
              });
            }
          } else {
            if (key !== 'pi' && key !== 'panelIds' && key !== 'userId' && key !== 'userName' && key !== 'userRole') {
              const element = this.userDetail[key];
              this.panelData.push({
                label: PanelObject[key].label,
                value: element
              });
            }
          }

        }
      }
    });
  }

  private createForm() {
    /* this.panelForm = this.formBuilder.group({
      low_pressure_switch_status: ['', [Validators.required]],
      panel_on_off: ['', Validators.required],
      over_voltage_tripping_voltage: ['', Validators.required],
      under_voltage_tripping_voltage: ['', Validators.required],
      raw_water_pump_overload_current: ['', Validators.required],
      raw_water_pump_dry_run_current: ['', Validators.required],
      high_pressure_pump_overload_current: ['', Validators.required],
      high_pressure_pump_dry_run_current: ['', Validators.required],
      panel_auto_manual: ['', Validators.required],
      panel_name: ['', Validators.required],
      isAllocated: [''],
    }); */
    this.panelForm = this.formBuilder.group({
      // lpss: [''],
      pst: [''],
      ovv: [''],
      uvv: [''],
      roc: [''],
      ruc: [''],
      hoc: [''],
      huc: [''],
      pop: [''],
      pn: ['', Validators.required],
      isAllocated: [''],
      im: [0]
    });
  }

  private setValue() {
    this.panelForm.patchValue(this.panelDetail);
  }

  ngOnInit() {
    this.role = this.commonService.checkUserRole();

    this.route.params.subscribe(params => {
      this.isEdit = params.id !== 'add';
      if (this.isEdit) {
        this.panelId = params.id;
        this.getPanelDataById(params.id);
      } else {
        if (!this.role.isAdmin) {
          this.router.navigate(['/core', 'panel']);
        }
      }

      this.createForm();
    });

    this.subscription = this.firebaseService.getPenelsData().subscribe(message => {
      this.getPanelDataById(this.panelId);
    });
  }

  savePanelForm() {
    if (this.panelForm.valid) {
      const data = this.panelForm.value;
      if (this.isEdit) {
        data.pid = this.panelId;
      }
      this.appService.addOrUpdatePanel(data).subscribe(res => {
        // this.getPanelDataById(this.panelId);
        this.router.navigate(['/core', 'panel']);
        if (!this.isEdit) {
          // this.panelForm.reset();
          this.commonService.showToaster('Panel Data added', 'info', 1000);
        } else {
          // this.getPanelDataById(this.panelId);
          this.commonService.showToaster('Panel Data updated', 'info', 1000);
        }
        this.commonService.scrollToTop();
      });
    } else {
      this.commonService.showToaster('Panel Data incomplete', 'warning', 1000);
    }
  }

  ngOnDestroy() {
    // this.socketService.disconnect();
  }

}
