import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { Utilities } from 'src/app/shared/services/utilities';
import { User, UserRoleCondition } from 'src/app/shared/modals/user';
import { PanelList } from 'src/app/shared/modals/panel-list';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  panelList: PanelList[];
  panelListAll: PanelList[];
  panelIds = [];
  userDetail: User;
  userId: string;
  userForm: FormGroup;
  isEdit: boolean;
  role: UserRoleCondition;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private appService: AppService,
    private commonService: CommonService,
    private formBuilder: FormBuilder) {
  }

  private getUserDetailById(id) {
    this.appService.getUserDetailById(id).subscribe(response => {
      this.userDetail = Object.assign({}, response);
      this.setValue();
    });
  }

  private createForm() {
    this.userForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      userEmail: ['', Validators.required],
      userPassword: ['', Validators.required]
    });
  }

  private setValue() {
    this.panelIds = [];
    this.userForm.patchValue(this.userDetail);
    if (this.userDetail.panelIds && this.userDetail.panelIds.length > 0 && this.panelListAll && this.panelListAll.length > 0) {
      this.userDetail.panelIds.forEach(element => {
        const data = this.panelListAll.find(x => x.pi === element);
        this.panelIds.push(data);
      });
    }
  }

  ngOnInit() {
    this.role = this.commonService.checkUserRole();
    this.route.data.subscribe(({ list }) => {
      this.panelList = list[1];
      this.panelListAll = list[0];
    });
    this.route.params.subscribe(params => {
      this.isEdit = params.id !== 'add';
      if (this.isEdit) {
        this.userId = params.id;
        this.getUserDetailById(params.id);
      } else {
        if (this.role.isEmployee) {
          this.router.navigate(['/core', 'dashboard']);
        }
      }
      this.createForm();
    });
  }

  panelsChange(value) {
    if (!Utilities.isEmptyObj(value)) {
      const data = this.panelList.find(x => x.pi === value);
      if (this.panelIds.findIndex(x => x.pi === data.pi) === -1) {
        this.panelIds.push(data);
      } else {
        this.commonService.showToaster('Ids are there', 'info', 1000);
      }
      value = '';
    }
  }

  removePanel(index) {
    this.panelIds.splice(index, 1);
  }

  saveUserForm() {
    if (this.role.isSupplier) {
      if (this.userForm.valid) {
        const data = this.userForm.value;
        if (this.isEdit) {
          data.userId = this.userId;
        }
        this.appService.addOrUpdateUser(data).subscribe(res => {
          // this.getUserDetailById(this.userId);
          // this.router.navigate(['/core', 'user']);
          // if (!this.isEdit) {
          //   this.userForm.reset();
          // } else {
          //   this.getUserDetailById(this.userId);
          // }
          // this.commonService.scrollToTop();
          if (this.isEdit) {
            // this.router.navigate(['/core', 'user', this.userId]);
            // window.location.reload();
          } else {
            // this.userForm.reset();
            // this.commonService.scrollToTop();
          }
          this.router.navigate(['/core', 'user']);

        });
      } else {
        this.commonService.showToaster('User Data incomplete', 'warning', 1000);
      }
    } else {
      if (this.userForm.valid) {
        const data = this.userForm.value;
        data.panelIds = this.panelIds.length > 0 ? this.panelIds.map(x => x.pi) : [];
        if (this.isEdit) {
          data.userId = this.userId;
        }
        this.appService.addOrUpdateUser(data).subscribe(res => {
          // this.getUserDetailById(this.userId);
          // this.router.navigate(['/core', 'user']);
          // if (!this.isEdit) {
          //   this.userForm.reset();
          // } else {
          //   this.getUserDetailById(this.userId);
          // }
          // this.commonService.scrollToTop();
          if (this.isEdit) {
            // this.router.navigate(['/core', 'user', this.userId]);
            // window.location.reload();
          } else {
            // this.userForm.reset();
            // this.commonService.scrollToTop();
          }
          this.router.navigate(['/core', 'user']);

        });
      } else {
        this.commonService.showToaster('User Data incomplete', 'warning', 1000);
      }
    }

  }

}
