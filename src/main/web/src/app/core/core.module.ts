import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from './core.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PanelDetailComponent } from './panel-detail/panel-detail.component';
import { CoreRoutingModule } from './core-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { PanelListComponent } from './panel-list/panel-list.component';
import { PanelListResolver } from './panel-list.resolver';

@NgModule({
  declarations: [
    CoreComponent,
    DashboardComponent,
    PanelDetailComponent,
    UserListComponent,
    UserDetailComponent,
    PanelListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[
    PanelListResolver
  ]
})
export class CoreModule { }
