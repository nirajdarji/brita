import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PanelDetailComponent } from './panel-detail/panel-detail.component';
import { PageNotFoundComponent } from '../shared/components/page-not-found/page-not-found.component';
import { CoreComponent } from './core.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { PanelListComponent } from './panel-list/panel-list.component';
import { PanelListResolver } from './panel-list.resolver';

const routes: Routes = [
    {
        path: '',
        component: CoreComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', component: DashboardComponent },
            { path: 'panel', component: PanelListComponent },
            { path: 'panel/:id', component: PanelDetailComponent },
            { path: 'user', component: UserListComponent },
            { path: 'user/:id', component: UserDetailComponent, resolve: { list: PanelListResolver } },
        ]
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class CoreRoutingModule { }
