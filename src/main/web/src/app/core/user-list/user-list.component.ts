import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { UserRoleCondition } from 'src/app/shared/modals/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userList: any[];
  role: UserRoleCondition;

  constructor(private appService: AppService, private commonService: CommonService) { }

  private getAllDataCall() {
    this.appService.getUsers().subscribe(response => {
      if (response.status) {
        this.userList = response.data;
      } else {
        this.commonService.showToaster(response.message, 'warning', 1000);
      }
    });
  }

  ngOnInit() {
    this.userList = [];
    this.role = this.commonService.checkUserRole();
    this.getAllDataCall();
  }

  removeUser(id) {
    this.appService.removeUserById(id).subscribe(response => {
      this.commonService.showToaster('User removed', 'info', 1000);
      this.getAllDataCall();
    });
  }
}
