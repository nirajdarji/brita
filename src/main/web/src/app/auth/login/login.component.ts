import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Validation, LocalStorage } from 'src/app/app.constant';
import { AppService } from 'src/app/app.service';
import { CommonService } from 'src/app/shared/services/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  validation: any = Validation;

  constructor(
    private fb: FormBuilder,
    private appService: AppService,
    private commonService: CommonService,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.commonService.getDataFromLocalStorageObject(LocalStorage.UserData, 'jwtToken')) {
      this.router.navigate(['core']);
    }
    this.createForm();
  }

  private createForm() {
    this.loginForm = this.fb.group({
      userName: ['', Validators.required],
      userPassword: ['', Validators.required],
      deviceToken: ['']
    });
  }

  checkInvalid(item): boolean {
    return this.loginForm.get(item).invalid && this.loginForm.get(item).touched;
  }

  loginCheck() {
    if (this.loginForm.valid) {
      this.loginForm.get('deviceToken').setValue(this.commonService.getLocalStorageObject(LocalStorage.Token));
      const data = this.loginForm.value;
      this.appService.userAuthenticate(data).subscribe(response => {
        if (response.status) {
          const lsData = {
            userId: response.data.userId,
            userName: response.data.userName,
            userRole: response.data.userRole,
            jwtToken: btoa(`${data.userName}:${data.userPassword}`)
          };
          this.commonService.setLocalStorageObject(LocalStorage.UserData, lsData);
          this.router.navigate(['core']);
        } else {
          this.commonService.showToaster(response.message, 'warning', 500);
        }
      });
    } else {
      this.commonService.showToaster('Opps..', 'info', 1000);
    }

  }

}
