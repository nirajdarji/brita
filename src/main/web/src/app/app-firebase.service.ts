import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { CommonService } from './shared/services/common.service';

@Injectable({
    providedIn: 'root'
})
export class AppFirebaseService {
    private subject = new Subject<any>();

    constructor(private commonService: CommonService) { }

    sendPenelsData(message: any) {
        this.subject.next({ text: message });
    }

    clearPenelsData() {
        this.subject.next();
    }

    getPenelsData(): Observable<any> {
        return this.subject.asObservable();
    }
}
