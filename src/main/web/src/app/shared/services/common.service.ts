import { Injectable } from '@angular/core';
import { Utilities } from './utilities';
import * as _ from 'lodash';
import { LocalStorage, UserRole } from 'src/app/app.constant';

declare var $;

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  setLocalStorageObject(key: string, value: any): void {
    window.localStorage.setItem(key, JSON.stringify(value));
  }

  getLocalStorageObject(key: string): any {
    const temp = window.localStorage.getItem(key);
    if (Utilities.isNull(temp)) {
      return null;
    }
    return JSON.parse(temp);
  }

  getDataFromLocalStorageObject(localkey: string, objkey: string): any {
    return Utilities.isNull(this.getLocalStorageObject(localkey)) ? null : this.getLocalStorageObject(localkey)[objkey];
  }

  clearLocalStorageObject(key: string) {
    window.localStorage.removeItem(key);
  }
  clearLocalStorageObjectAll() {
    window.localStorage.clear();
  }

  showToaster(msg: string, style: string, time: number) {
    $.notify({ message: msg }, {
      type: style || 'info',
      timer: time || 4000
    });
  }

  checkUserRole() {
    const data = this.getDataFromLocalStorageObject(LocalStorage.UserData, 'userRole');
    return {
      isAdmin: data === UserRole.admin,
      isEmployee: data === UserRole.employee,
      isSupplier: data === UserRole.supplier,
    };
  }

  scrollToTop() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 50); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }

}
