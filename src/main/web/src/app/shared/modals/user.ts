export class User {
    userId: string;
    userName: string;
    userEmail: string;
    userPassword: string;
    userRole: string;
    creator: string;
    panelIds: string[];
}

export class UserRoleCondition {
    isAdmin: boolean;
    isEmployee: boolean;
    isSupplier: boolean;
}
