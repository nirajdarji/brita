import { Component, OnInit } from '@angular/core';

import * as firebase from 'firebase/app';
import 'firebase/messaging';
import { FirebaseConfig, LocalStorage } from './app.constant';
import { CommonService } from './shared/services/common.service';
import { AppFirebaseService } from './app-firebase.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'my-dream-app';

  constructor(
    private commonService: CommonService,
    private appFirebaseService: AppFirebaseService
  ) { }

  ngOnInit() {
    this.initFirebase();
    // this.appFirebaseService.requestPermission();
  }

  initFirebase() {
    firebase.initializeApp(FirebaseConfig);

    const messaging = firebase.messaging();
    messaging.usePublicVapidKey(
      'BC4ntfNl51usbJ9w-r3_bdObv4BWAvJrUJvQqIQyPYqrrruQIRVkBiW2a3EIMLdG_NiPlEFAJde6OTF7Du3sqTw'
    );
    messaging
      .requestPermission()
      .then(() => {
        console.log('Permission Granted');
        messaging
          .getToken()
          .then(currentToken => {
            this.commonService.setLocalStorageObject(LocalStorage.Token, currentToken);
          })
          .catch(err => {
            console.log('An error occurred while retrieving token. ', err);
          });
      })
      .catch(err => {
        console.log('Error', err);
      });

    messaging.onMessage(payload => {
      this.appFirebaseService.sendPenelsData(payload);
    });
  }
}
